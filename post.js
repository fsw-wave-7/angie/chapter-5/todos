const {succesResponse}  = require("./response.js")

class Todo {
    constructor() {
        this.todo = [] 
    }

    getTodo = (req, res) =>{
        succesResponse(
            res,
            200,
            this.todo,
            {total: this.todo.length});  
    }

    insertTodo =(req, res)=>{
        const body = req.body
        
        const param = {
            'name': body.name,
            'isDone': body.isDone
        }
        this.todo.push(param)
        succesResponse(res,201,param)
    }

    updateTodo =(req, res)=>{
        const index = req.params.index
        const body = req.body

        this.todo[index].name = body.name
        this.todo[index].isDone = body.isDone
        
        succesResponse(res,200, this.todo[index])
    }

    deleteTodo =(req, res)=>{
        const index = req.params.index   
        this.todo.splice(index, 1)
        succesResponse(res, 200, null)
    }
 }

module.exports = Todo

const express = require('express');
const app = express();
const port = 3000;
const Todo = require ('./post.js')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const todo = new Todo

app.use(jsonParser)

app.get('/todo', todo.getTodo)
app.post('/todo',todo.insertTodo)
app.put('/done/:index', todo.updateTodo)
app.delete('/todo/:index', todo.deleteTodo)

app.listen(port, () => { console.log("Server Berhasil dijalankan"); });

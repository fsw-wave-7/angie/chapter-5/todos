const messages = {
    '200': 'sukses',
    '201': 'Data berhasil disimpan'
}

function succesResponse(
    res,
    code,
    data,
    meta ={}
){
    res.status(code).json({
        data: data,
        meta: {
            code: code,
            message: messages[code.toString()],
            ...meta //spread operator
        }
    })
}

module.exports = {
    succesResponse
}
